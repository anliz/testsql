#a) Los Clientes que arriendan la casa ubicada en la calle "Calle 29 #26-75 Belen"

select c.nombre, c.apellido from cliente as c 
join arriendo as a 
on c.documento =a.documento
join casa as ca
on a.id_casa= ca.id
where (Calle='Calle 29') and (Nro='#26-75') and(Barrio='Belen');

# b) Cuanto dinero le deben a Maria Lizarazo

select sum(a.Valor_Deuda) from propietario as p
join casa as ca
on p.documento = ca.documento
join arriendo as a
on ca.id = a.id_casa
where (Nombre= 'Maria')and(Apellido= 'Lizarazo');

#c) Cuanto dinero en total se le debe A CADA DUEÑO

select p.nombre, sum(a.Valor_Deuda) as deudaTotal from propietario as p
join casa as ca
on p.documento = ca.documento
join arriendo as a
on ca.id = a.id_casa
group by p.documento;

#d) Liste todas las personas de la base de datos

select nombre, apellido from propietario 
union all select nombre, apellido from cliente;

#e) Liste los propietarios que tienen 3 o más casas.

select p.nombre, count(c.id) as cantidad from casa c
join propietario p
on c.documento= p.documento
group by p.nombre
having count(c.id)>=3 ;

 # Liste los propietarios que tienen valor deuda ( mayor a CERO ) en todas sus casas

select distinct concat(p.nombre, '  ', p.apellido) as propietarios from propietario p
join casa c
on p.documento = c. documento
join arriendo a
on c.id= a.id_casa
where valor_deuda > 0;

#g) Por cada propietario y sus casas liste por favor:
#1. El promedio de casas arrendadas
select p.nombre, count(c.id) as cantidad from casa c
join propietario p
on c.documento= p.documento
group by p.nombre;

#2. La casa con MAYOR deuda

select c.documento, max(valor_deuda)as mayorDeuda from casa c 
join arriendo a
on c.id= a.id_casa;

#La casa con MENOR deuda El mínimo.

select c.documento, min(valor_deuda)as mayorDeuda from casa c 
join arriendo a
on c.id= a.id_casa;
