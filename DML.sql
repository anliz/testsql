
# INSERTAR REGISTRO A LA TABLA CLIENTE

insert into cliente value(1324523,'Angie','Cardenas');
insert into cliente value(1233422,'Maria','Lozano');
insert into cliente value(1324522,'Sofia','Palacio');
insert into cliente value(1324554,'Camilo','Salcedo');
insert into cliente value(5324523,'Samanta','Caceres');
insert into cliente value(6324525,'Andres','Ortega');
insert into cliente value(4324526,'Francisco','Perez');
insert into cliente value(13245222,'Francisco','Gonzales');

# INSERTAR REGISTRO A LA TABLA ARRIENDO

insert into arriendo value(1324523, 01, 600000);
insert into arriendo value(1233422,'03',80000);
insert into arriendo value(1324522,'02',100000);
insert into arriendo value(1324554,'04',200000);
insert into arriendo value(5324523,'05',500000);
insert into arriendo value(6324525,'06',50000);
insert into arriendo value(4324526,'07',100000);
insert into arriendo value(13245222, 01, 700000);

# INSERTAR REGISTRO A LA TABLA TELEFONOS

insert into telefonos value(1233422,'23452345023');
insert into telefonos value(1324523,'53245345789');  
insert into telefonos value(1324522,'46575345789');
insert into telefonos value(1324554,'88945345789');
insert into telefonos value(5324523,'67245345789');
insert into telefonos value(6324525,'76245345567');
insert into telefonos value(3478343,'28372346292');
insert into telefonos value(3478342,'49538345343');
insert into telefonos value(3478345,'6753834455');

# INSERTAR REGISTRO A LA TABLA PROPIETARIO

insert into propietario value(3478343,'Carmen','Perez');
insert into propietario value(3478342,'Maria','Lizarazo');
insert into propietario value(643223,'Juan','Cardenaz');
insert into propietario value(3478345,'Santigo','Villamizar');

# INSERTAR REGISTRO A LA TABLA CASA

insert into casa value(01,3478342,'Calle 29','#26-75','Belen');
insert into casa value(02,3478343,'Calle 25','#25-78','Los Patios');
insert into casa value(03,643223,'av 2','#3-74','La playa');
insert into casa value(04,3478345,'Calle 10','#26-32','El Contento');
insert into casa value(05,643223,'av 0','#5-43','Centro');
insert into casa value(06,3478345,'Calle 28','#27-88','Belen');
insert into casa value(07,3478343,'Calle 31','#33-35','Atalaya');
